package timer

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Timer struct {
	times [7][24]uint64
}

func New() *Timer {
	return &Timer{}
}

// check_whm ensures that the weekday, hour and minute values
// are in the proper ranges:  0-6, 0-59 and 0-59 and returns
// a non nil error if they are not
func check_whm(weekday, hour, minute uint) (err error) {
	if weekday < 0 || weekday > 6 {
		return errors.New("Weekday out of range")
	}
	if hour < 0 || hour > 59 {
		return errors.New("Hour out of range")
	}
	if minute < 0 || minute > 59 {
		return errors.New("Minute out of range")
	}
	return
}

//AddDHM adds a timer detent for a particular weekday, hour and minute
func (t *Timer) AddDHM(weekday, hour, minute uint) (err error) {
	err = check_whm(weekday, hour, minute)
	if err != nil {
		return
	}
	var bit uint64 = (1 << minute)
	t.times[weekday][hour] |= bit
	return
}

// AddDHMrange adds detents based on start and end ranges for weekday, hour and
// minute.
func (t *Timer) AddDHMrange(dowstart, dowend, hourstart, hourend, minstart, minend uint) (err error) {
	err = check_whm(dowstart, hourstart, minstart)
	if err != nil {
		return
	}
	err = check_whm(dowend, hourend, minend)
	if err != nil {
		return
	}
	if dowstart > dowend {
		dowstart, dowend = dowend, dowstart
	}
	if hourstart > hourend {
		hourstart, hourend = hourend, hourstart
	}
	if minstart > minend {
		minstart, minend = minend, minstart
	}
	for d := dowstart; d <= dowend; d++ {
		for h := hourstart; h <= hourend; h++ {
			for m := minstart; m <= minend; m++ {
				err = t.AddDHM(d, h, m)
				if err != nil {
					return
				}
			}
		}
	}
	return
}

func bitCountUint64(val uint64) (count int) {
	var x uint
	for x = 0; x < 64; x++ {
		if val&(1<<x) != 0 {
			count += 1
		}
	}
	return
}

func (t *Timer) Print() {
	fmt.Printf("dow: ")
	for h := 0; h < 24; h++ {
		fmt.Printf(" %2.2d", h)
	}
	fmt.Printf("\n")
	for d := 0; d < 7; d++ {
		fmt.Printf(" %d : ", d)
		for h := 0; h < 24; h++ {
			c := bitCountUint64(t.times[d][h])
			if t.times[d][h] != 0 {
				fmt.Printf(" %2.2d", c)
			} else {
				fmt.Printf("   ")
			}
		}
		fmt.Printf("\n")
	}
}

// parseintrange parses a string in the form of x-y or x where
// x and y are integers and must be in the range [lb,ub]
func parseintrange(str string, lb uint, ub uint) (s uint, e uint, err error) {
	var x, y uint64
	r := strings.Split(str, "-")
	lr := len(r)
	if lr < 1 || lr > 2 {
		return 0, 0, errors.New("parseintrange: Range error")
	}
	x, err = strconv.ParseUint(r[0], 10, 32)
	if err != nil {
		return
	}
	s = uint(x)
	if s < lb || s > ub {
		return 0, 0, errors.New("parseintrange: Start value out of bounds")
	}
	if lr == 1 {
		return s, s, err
	}
	y, err = strconv.ParseUint(r[1], 10, 32)
	if err != nil {
		return
	}
	e = uint(y)
	if e < lb || e > ub {
		return 0, 0, errors.New("parseintrange: End value out of bounds")
	}
	return s, e, err
}

// ParseRange adds timer detents based on a string description of the
// following form: <weekdayspec>:<hourspec>:<minutespec> where each
// field specification can contain multiple ranges.  for example
// 1,2,3,4,5:9,11,13,15,17:0 will add detents at be beginning of every other
// hour between 9-17 on weekdays monday - friday
func (t *Timer) ParseRange(spec string) (err error) {
	s := strings.Split(spec, ":")
	if len(s) != 3 {
		return errors.New("Invalid range spec.  Must include dow, hour and minute.")
	}
	var ds, de, hs, he, ms, me uint
	for _, d := range strings.Split(s[0], ",") {
		ds, de, err = parseintrange(d, 0, 6)
		if err != nil {
			return
		}
		for _, h := range strings.Split(s[1], ",") {
			hs, he, err = parseintrange(h, 0, 59)
			if err != nil {
				return
			}
			for _, m := range strings.Split(s[2], ",") {
				ms, me, err = parseintrange(m, 0, 59)
				if err != nil {
					return
				}
				err = t.AddDHMrange(ds, de, hs, he, ms, me)
				if err != nil {
					return
				}
			}
		}
	}
	return
}

func (t *Timer) IsDayHourMin(weekday, hour, minute uint) bool {
	var bit uint64 = (1 << minute)
	if t.times[weekday][hour]&bit != 0 {
		return true
	}
	return false
}

func (t *Timer) IsTime(ts time.Time) bool {
	weekday := ts.Weekday()
	hour, minute, _ := ts.Clock()
	return t.IsDayHourMin(uint(weekday), uint(hour), uint(minute))
}

func (t *Timer) IsNow() bool {
	ts := time.Now().Local()
	_, _, sec := ts.Clock()
	if sec != 0 {
		return false
	}
	return t.IsTime(ts)
}
